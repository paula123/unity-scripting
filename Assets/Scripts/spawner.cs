﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {

    public GameObject hijitos;
    public float fuerzaMin, fuerzaMax;

	void crearObjeto() {

        GameObject hijo = Instantiate(hijitos, transform.localPosition, Quaternion.identity);
        float escalax = Random.Range(1f, 10);
        float escalay = Random.Range(1f, 10);
        float escalaz = Random.Range(1f, 10);
        float fuerzax = Random.Range(fuerzaMin, fuerzaMax);
        float fuerzay = Random.Range(fuerzaMin, fuerzaMax);
        float fuerzaz = Random.Range(fuerzaMin, fuerzaMax);
        float rojo = Random.Range(0f, 1);
        float verde = Random.Range(0f, 1);
        float azul = Random.Range(0f, 1);
        float tiempo = Random.Range(0.5f, 3);

        hijo.transform.localScale = new Vector3 (escalax, escalay, escalaz);
        hijo.GetComponent<Rigidbody>().AddForce (new Vector3(fuerzax, fuerzay, fuerzaz));
        hijo.GetComponent<MeshRenderer>().material.color = new Color(rojo, verde, azul);

        Invoke("crearObjeto",tiempo);
        Debug.Log("esto funciona");
        	
	}
	

	void Start () {

        float tiempo = Random.Range(0.5f, 3);
        Invoke("crearObjeto", tiempo);

    }
}
