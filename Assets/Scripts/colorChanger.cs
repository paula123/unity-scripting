﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colorChanger : MonoBehaviour {

    public Material material;
    public Color viejoColor;

    Color ColorNuevo() {
        float rojo;
        float azul;
        float verde;
        rojo = Random.Range(0f, 1);
        azul = Random.Range(0f, 1);
        verde = Random.Range(0f, 1);
        
        return new Color(rojo, verde, azul);
    }
      
	void Update () {

        if (Input.GetKeyDown(KeyCode.C)) {

            material.color = ColorNuevo();

        }

        if (Input.GetKeyUp(KeyCode.C)) {

            material.color = viejoColor;
        }
		
	}
}
