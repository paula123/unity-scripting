﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovement : MonoBehaviour
{

    public GameObject invis;
    public Transform centro;
    public Vector3 eje = Vector3.up;
    public Vector3 posicion;
    public float radio = 2.0f;
    public float velRadio = 0.5f;
    public float velRotacion = 80.0f;

    void Start()
    {
        centro = invis.transform;
        transform.position = (transform.position - centro.position).normalized * radio + centro.position;
        radio = 2.0f;
    }

    void Update()
    {
        transform.RotateAround(centro.position, eje, velRotacion * Time.deltaTime);
        posicion = (transform.position - centro.position).normalized * radio + centro.position;
        transform.position = Vector3.MoveTowards(transform.position, posicion, Time.deltaTime * velRadio);
    }
}